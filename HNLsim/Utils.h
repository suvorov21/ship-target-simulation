#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <sstream>
#include <map>


//ROOT
#include <TVector3.h>
#include <TLorentzVector.h>
#include <utility>
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TString.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TAxis.h"

#include "Units.h"

namespace utils {

  class TRay{
    public:
      /// a constructor given a position and direction
      TRay(const TVector3&, const TVector3&);

      /// a default destructor
      virtual ~TRay() {};

      const TVector3& GetPos()    const {return pos__;}
      const TVector3& GetDir()    const {return dir__;}
      const TVector3& GetInvDir() const {return invDir__;}

      TVector3 Propagate(Double_t distance) const {return pos__ + distance*dir__;}


    private:
      TVector3 pos__;
      TVector3 dir__;
      TVector3 invDir__;
  };

  class TRectangle{
    public:
      /// constructor provided normal, "main" (U) axis and sizes
      TRectangle(const TVector3&, const TVector3&, const TVector3&,
          Double_t, Double_t);

      /// default destructor
      virtual ~TRectangle() {};

      /// return true if the point is inside the rectangle
      bool IsInside(const TVector3&) const;

      const TVector3& GetPos() const {return pos__;}

      Double_t GetU() const {return U__;}
      Double_t GetV() const {return U__;}

      const TVector3& GetUaxis() const {return vaxis__;}
      const TVector3& GetVaxis() const {return uaxis__;}

      const TVector3& GetNormal() const {return norm__;}

      /// return closest distance from the point to the plane
      Double_t ClosestDistance(const TVector3& r0) const {
        return (pos__ - r0).Dot(norm__);
      }

      /// wether a ray intersect this plane
      bool Intersect(const TRay&, Double_t&) const;

    private:

      Double_t U__;
      Double_t V__;

      TVector3 pos__;
      TVector3 norm__;

      TVector3 uaxis__;
      TVector3 vaxis__;
  };

  class TElipse{
    public:
      /// constructor provided normal, "main" (U) axis and sizes
      TElipse(const TVector3&, const TVector3&, const TVector3&,
          Double_t, Double_t);

      /// default destructor
      virtual ~TElipse() {};

      /// return true if the point is inside the rectangle
      bool IsInside(const TVector3&) const;

      const TVector3& GetPos() const {return pos__;}

      Double_t GetU() const {return U__;}
      Double_t GetV() const {return U__;}

      const TVector3& GetUaxis() const {return vaxis__;}
      const TVector3& GetVaxis() const {return uaxis__;}

      const TVector3& GetNormal() const {return norm__;}

      /// return closest distance from the point to the plane
      Double_t ClosestDistance(const TVector3& r0) const {
        return (pos__ - r0).Dot(norm__);
      }

      /// wether a ray intersect this plane
      bool Intersect(const TRay&, Double_t&) const;

    private:

      Double_t U__;
      Double_t V__;

      TVector3 pos__;
      TVector3 norm__;

      TVector3 uaxis__;
      TVector3 vaxis__;
  };

  /*class TElips_channel{
    public:
      /// constructor provided normal, "main" (U) axis and sizes
      TElips_channel(const TVector3&, const TVector3&, const TVector3&,
          Double_t, Double_t, Double_t);

      /// default destructor
      virtual ~TElips_channel() {};

      /// wether a ray intersect this box
      bool Intersect(const TRay&, Double_t&, Double_t&) const;

    private:

      Double_t U__;
      Double_t V__;
      Double_t Z__;

      TVector3 pos__;
      TVector3 norm__;

      TVector3 uaxis__;
      TVector3 vaxis__;
  };*/

  /// this is a simple box in 3D, for the moment axis aligned one (as is ND280 complex)
  class TBox3D_AAB{
    public:
      /// a constructor given a position of the center and half-sizes for each side
      TBox3D_AAB(const TVector3&, Double_t, Double_t, Double_t);

      /// default destructor
      virtual ~TBox3D_AAB() {};

      const TVector3& GetPos() const {return pos__;}

      Double_t GetDx() const {return Dx__;}
      Double_t GetDy() const {return Dy__;}
      Double_t GetDz() const {return Dz__;}

      const TVector3& GetMin() const {return min__;}
      const TVector3& GetMax() const {return max__;}

      /// wether a ray intersect this box
      bool Intersect(const TRay&, Double_t&, Double_t&) const;

    private:

      /// position of the center
      TVector3 pos__;

      /// half sizes
      Double_t Dx__;
      Double_t Dy__;
      Double_t Dz__;

      /// edge coordinates of the box
      TVector3 min__;
      TVector3 max__;
  };

  /// width of charged meson decay into charged lepton and heavy neutral one
  /// this is pure "kinematics", no additional mixing included
  /// requires: meson mass, form-factor, cabbibo-mixing element, lepton mass and heavy lepton mass
  /// arxiv:0705.1729
  Double_t WidthMesonToHNuLepton(Double_t, Double_t, Double_t, Double_t, Double_t);

  /// width of 2-body decay of the neutral lepton (heavy neutrino)
  /// pure kinematics, mixing element is not considered
  /// requires: heavy nu mass, form-factor, cabbibo mixing element, lepton mass and meson mass
  /// arxiv:0705.1729
  Double_t WidthHNuToMesonLepton(Double_t, Double_t, Double_t, Double_t, Double_t);

  /// width of 3-body decay of the neutral lepton (heavy neutrino)
  /// di-lepton case: two particle & anti-particle leptons and a neutrino
  /// pure kinematics
  /// requires: heavy nu mass, leton mass, whether all have same flavor
  Double_t WidthHNuToDiLeptonNu(Double_t, Double_t, bool);

  /// width of 3-body decay of the neutral lepton (heavy neutrino)
  /// lepton+lepton+neutrino (same flavor as one lepton) case: lepton1 != lepton2
  /// pure kinematics
  /// requires: heavy nu mass, first leton mass, second lepton mass
  Double_t WidthHNuTo2LeptonNu(Double_t, Double_t, Double_t);

}

#endif
