#include "Utils.h"

#include <cmath>
#include <dirent.h>
#include <iostream>
#include <fstream>
#include "TMath.h"

void Utils() {
  (void)1;
}

namespace utils {
  /*!
   * ctor
   */
  TRay::TRay(const TVector3& pos, const TVector3& dir){
    dir__ = dir.Unit();
    pos__ = pos;

    //inverse direction
    Double_t x = dir__.X()!=0 ? 1/dir__.X() : DBL_MAX;
    Double_t y = dir__.Y()!=0 ? 1/dir__.Y() : DBL_MAX;
    Double_t z = dir__.Z()!=0 ? 1/dir__.Z() : DBL_MAX;

    invDir__.SetXYZ(x,y,z);


  }

  /*!
   * ctor
   */
  TRectangle::TRectangle(const TVector3& pos, const TVector3& norm, const TVector3& uaxis,
      Double_t U, Double_t V){

    norm__ = norm.Unit();

    U__ = fabs(U);
    V__ = fabs(V);

    pos__ = pos;

    uaxis__ = uaxis.Unit();
    vaxis__ = norm__.Cross(uaxis__);


  }


  bool TRectangle::IsInside(const TVector3& r0) const {

    TVector3 res = (r0  - pos__);

    if (ClosestDistance(r0) > units::tolerance)
      return false;
    else if ( fabs(res.Dot(uaxis__)) > U__  )
      return false;
    else if ( fabs(res.Dot(vaxis__)) > V__ )
      return false;


    return true;
  }

  bool TRectangle::Intersect(const TRay& ray, Double_t& distance) const{

    distance = units::kUnassigned;

    TVector3 u  = ray.GetDir();
    TVector3 r0 = ray.GetPos();

    u = u.Unit();

    Double_t cos_theta = u.Dot(norm__);
    if (fabs(cos_theta) == 0) return false;

    distance = ClosestDistance(r0)/cos_theta;

    return IsInside(r0 + distance*u);
  }


  TElipse::TElipse(const TVector3& pos, const TVector3& norm, const TVector3& uaxis,
      Double_t U, Double_t V){

    norm__ = norm.Unit();

    U__ = fabs(U);
    V__ = fabs(V);

    pos__ = pos;

    uaxis__ = uaxis.Unit();
    vaxis__ = norm__.Cross(uaxis__);


  }

  bool TElipse::IsInside(const TVector3& r0) const {

    TVector3 res = (r0  - pos__);

    if (ClosestDistance(r0) > units::tolerance)
      return false;

    Double_t x = res.Dot(uaxis__);
    Double_t y = res.Dot(vaxis__);
    if (x*x/U__/U__ + y*y/V__/V__ <= 1)
      return true;

    return false;
  }


  bool TElipse::Intersect(const TRay& ray, Double_t& distance) const{

    distance = units::kUnassigned;

    TVector3 u  = ray.GetDir();
    TVector3 r0 = ray.GetPos();

    u = u.Unit();

    Double_t cos_theta = u.Dot(norm__);
    if (fabs(cos_theta) == 0) return false;

    distance = ClosestDistance(r0)/cos_theta;

    return IsInside(r0 + distance*u);
  }


  /*TElips_channel::TElips_channel(const TVector3& pos, const TVector3& norm, const TVector3& uaxis,
      Double_t U, Double_t V, Double_t Z){

    norm__ = norm.Unit();

    U__ = fabs(U);
    V__ = fabs(V);
    Z__ = fabs(Z);

    pos__ = pos;

    uaxis__ = uaxis.Unit();
    vaxis__ = norm__.Cross(uaxis__);


  }

  bool TElips_channel::Intersect(const TRay& ray, Double_t& t_min, Double_t& t_max) const {

    TVector3 u  = ray.GetDir();
    TVector3 r0 = ray.GetPos();

    std::vector<Double_t> intersections;

    double k_front = abs(pos__.Z() - r0.Z()) / u.Z();
    double k_back = abs(pos__.Z() + Z__ - r0.Z()) / u.Z();

    TElipse el_front(pos__, norm__, uaxis__, U__, V__);
    if (el_front.IsInside(r0+u*k_front))
      intersections.push_back(k_front);

    TElipse el_back(TVector3(pos__.X(), pos__.Y(), pos__.Z() + Z__), norm__, uaxis__, U__, V__);
    if (el_front.IsInside(r0+u*k_back))
      intersections.push_back(k_back);

    Double_t x = u.Dot(uaxis__);
    Double_t y = u.Dot(vaxis__);

    TVector3 v_plane = TVector3(x, y, 0.);

    Double_t a = V__*V__ * x*x + U__*U__ * y*y;
    Double_t b = 2*V__*V__ * x*x + r0.X()*r0.X() + 2*U__*U__ * y*y * r0.Y()*r0.Y();
    //Double_t c =



    return false;
  }*/


  TBox3D_AAB::TBox3D_AAB(const TVector3& pos, Double_t Dx, Double_t Dy, Double_t Dz ){
    pos__   = pos;

    Dx__  = fabs(Dx);
    Dy__  = fabs(Dy);
    Dz__  = fabs(Dz);

    // get the edges
    min__ = TVector3(pos - TVector3(Dx__, Dy__, Dz__));
    max__ = TVector3(pos + TVector3(Dx__, Dy__, Dz__));
  }


  bool TBox3D_AAB::Intersect(const TRay& ray, Double_t& t_min, Double_t& t_max) const {

    t_min = units::kUnassigned;
    t_max = units::kUnassigned;

    Double_t tmin, tmax, tymin, tymax, tzmin, tzmax;

    // X
    tmin = (min__.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    tmax = (max__.X() - ray.GetPos().X()) * ray.GetInvDir().X();
    if (tmin > tmax) std::swap(tmin, tmax);

    // Y
    tymin = (min__.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();
    tymax = (max__.Y() - ray.GetPos().Y()) * ray.GetInvDir().Y();

    if (tymin > tymax) std::swap(tymin, tymax);
    if ((tmin > tymax) || (tymin > tmax))
      return false;

    if (tymin > tmin)
      tmin = tymin;
    if (tymax < tmax)
      tmax = tymax;

    // Z
    tzmin = (min__.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    tzmax = (max__.Z() - ray.GetPos().Z()) * ray.GetInvDir().Z();
    if (tzmin > tzmax) std::swap(tzmin, tzmax);

    if ((tmin > tzmax) || (tzmin > tmax))
      return false;

    if (tzmin > tmin)
      tmin = tzmin;
    if (tzmax < tmax)
      tmax = tzmax;

    //set the output values
    t_min =  tmin;

    t_max = tmax;

    return true;
  }

  Double_t WidthMesonToHNuLepton(Double_t Mh, Double_t fh, Double_t Vc, Double_t Ml, Double_t Mnu){

    long double Mnu2 = Mnu*Mnu;
    long double Mh2  = Mh*Mh;
    long double Ml2  = Ml*Ml;


    long double A = 1. - Mnu2/Mh2 + 2.*Ml2/Mh2 + (Ml2/Mnu2)*(1. - Ml2/Mh2);

    long double B = sqrt(pow((1. + Mnu2/Mh2 - Ml2/Mh2),2) - 4.*Mnu2/Mh2);

    long double factor  = (Vc*Vc)*((units::Gfermi*units::Gfermi)*(fh*fh)*Mh)/(8.*TMath::Pi());


    //if mass is very small
    if(Mnu<units::min_mass){
      return   factor*Ml2*(1 - Ml2/Mh2)*(1 - Ml2/Mh2)*units::inv_hplanck;

    }

    return factor*(Mnu*Mnu)*A*B*units::inv_hplanck;

  }

  Double_t WidthHNuToMesonLepton(Double_t Mnu, Double_t fh, Double_t Vc, Double_t Ml, Double_t Mh){

    long double Mnu2 = Mnu*Mnu;
    long double Mh2  = Mh*Mh;
    long double Ml2  = Ml*Ml;

    long double A = pow((1. - Ml2/Mnu2),2) - Mh2/Mnu2*(1. + Ml2/Mnu2);

    long double B = sqrt((1. - pow((Mh - Ml),2)/Mnu2)*(1.-pow((Mh + Ml),2)/Mnu2));

    long double factor  = ((units::Gfermi*units::Gfermi)*(Vc*Vc)*(fh*fh)*pow(Mnu,3))/(16.*TMath::Pi());

    return factor*A*B*units::inv_hplanck;

  }

  Double_t WidthHNuToDiLeptonNu(Double_t Mnu, Double_t Ml, bool same_flavor){

    if(Mnu<2*Ml)
      std::cout<<"utils::WidthHNuToDiLeptonNu() HNL mass smaller than 2*mass_lepton \n"
        << "cannot proceed!" <<std::endl;

    long double factor  = (units::Gfermi*units::Gfermi)*std::pow(Mnu, 5)/(192.*std::pow(TMath::Pi(), 3));

    long double C1 = 0.25*(1 - 4*units::sin_thw2 + 8.*pow(units::sin_thw2,2.));

    long double C2 = 0.5*units::sin_thw2*(2.*units::sin_thw2 - 1);

    long double C3 = 0.25*(1+4*units::sin_thw2+8*pow(units::sin_thw2,2.));

    long double C4 = 0.5*units::sin_thw2*(2.*units::sin_thw2 + 1);

    Int_t delta_ab = (int)same_flavor;


    long double xl  = Ml/Mnu;
    long double xl2 = xl*xl;
    long double xl4 = xl2*xl2;
    long double xl6 = xl4*xl2;

    long double L1 = (1/xl2)*(1. - 3.*xl2 - (1. - xl2)*(std::sqrt(1. - 4.*xl2)));
    long double L2 = (1. + std::sqrt(1. - 4.*xl2));
    long double L  = std::log(L1) - std::log(L2);

    long double main13 = C1*(1 - delta_ab) + C3*delta_ab;
    main13 *= (1.-14.*xl2-2.*xl4-12.*xl6)*std::sqrt(1. - 4.*xl2) + 12.*xl4*(xl4-1.)*L;

    long double main24 = 4.*(C2*(1 - delta_ab)+C4*delta_ab);
    main24 *= xl2*(2. + 10.*xl2 - 12.*xl4)*std::sqrt(1. - 4.*xl2)+6.*xl4*(1 - 2.*xl2 + 2*xl4)*L;

    return factor*(main13 + main24)*units::inv_hplanck;;
  }

  Double_t WidthHNuTo2LeptonNu(Double_t Mnu, Double_t Ml1, Double_t Ml2){

    if(Mnu<Ml1+Ml2)
      std::cout<<"utils::WidthHNuToDiLeptonNu() HNL mass smaller than sum of lepton masses \n"
        << "cannot proceed!" <<std::endl;

    long double factor  = (units::Gfermi*units::Gfermi)*std::pow(Mnu, 5)/(192.*std::pow(TMath::Pi(), 3));

    long double xl = std::max(Ml1, Ml2);
    xl /= Mnu;

    long double xl2 = xl*xl;
    long double xl4 = xl2*xl2;
    long double xl6 = xl4*xl2;
    long double xl8 = xl4*xl4;


    long double main = 1 -  8.*xl2 + 8*xl6 - xl8 - 12.*xl4*std::log(xl2);

    return factor*main*units::inv_hplanck;
  }

} //namespace
