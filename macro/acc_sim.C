#include <iostream>

#include "TRandom3.h"
#include "TVector3.h"
#include "TLorentzVector.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"

using namespace std;

float GetProdE(float Mp, float M1, float M2) {
  return (Mp*Mp + M1*M1 - M2*M2) / (2*Mp);
}

bool Inside(float x, float y) {
  return (x*x/(2.5*2.5) + y*y/(5*5) < 1);
}

bool IntersectEnd(TVector3 pos, TVector3 dir) {
  float t = (50. - pos.Z()) / dir.Z();
  if (t < 0.)
    return false;

  TVector3 end( pos.X() + t * dir.X(),
                pos.Y() + t * dir.Y(),
                pos.Z() + t * dir.Z());
  return Inside(end.X(), end.Y());
}

void acc_sim() {

  float M_hnl = 200.;

  float M_mu  = 105.6584;
  float M_pi  = 139.57061;
  float M_e   = 0.5488;
  float M_k   = 493.677;

  int N_sim     = 100000000;
  int N_events  = 0;
  int N_sel     = 0;

  // production mode
  float Ehnl = GetProdE(M_k, M_hnl, M_e);
  float Phnl = sqrt(Ehnl*Ehnl - M_hnl*M_hnl);
  float beta = Phnl / Ehnl;

  TRandom3 gRand(0);

  // decay mode
  float M1 = M_e;
  float M2 = M_pi;

  TFile f("out.root", "RECREATE");

  float E1_0 = GetProdE(M_hnl, M1, M2);
  float E2_0 = GetProdE(M_hnl, M2, M1);

  TH2F* h2d   = new TH2F("diplot", "diplot", 100, 0., 1000., 100, 0., 1000.);
  TH2F* h2dt  = new TH2F("diplot_t", "diplot", 100, 0., 1000., 100, 0., 1000.);

  for (auto i = 0; i < N_sim; ++i) {
    float x = (gRand.Uniform() - 1) * 5;
    float y = (gRand.Uniform() - 1) * 10;
    float z = gRand.Uniform() * 50.;

    if (!Inside(x, y))
      continue;

    ++N_events;

    TLorentzVector v1, v2;

    float phi, cosTheta, sinTheta;
    phi       = gRand.Uniform(1.) * 2 * TMath::Pi();
    cosTheta  = gRand.Uniform(1.) * 2 - 1;
    sinTheta  = sqrt(1 - cosTheta*cosTheta);

    v1.SetE(E1_0);
    float p = sqrt(E1_0*E1_0 - M1*M1);
    TVector3 mom(p * sinTheta * TMath::Cos(phi), sinTheta * TMath::Sin(phi) * p, cosTheta * p);
    v1.SetPx(mom.x());
    v1.SetPy(mom.y());
    v1.SetPz(mom.z());

    v2.SetE(E2_0);
    p = sqrt(E2_0*E2_0 - M2*M2);
    mom = TVector3(-p * sinTheta * TMath::Cos(phi), -sinTheta * TMath::Sin(phi) * p, -cosTheta * p);
    v2.SetPx(mom.x());
    v2.SetPy(mom.y());
    v2.SetPz(mom.z());

    h2dt->Fill(v1.P(), v2.P());

    v1.Boost(0., 0., beta);
    v2.Boost(0., 0., beta);

    TVector3 pos(x, y, z);

    //cout << pos.X() << "\t" << pos.Y() << "\t" << pos.Z() << endl;
    //cout << v1.Vect().Theta() << "\t" << v2.Vect().Theta() << endl;

    if (IntersectEnd(pos, v1.Vect()) && IntersectEnd(pos, v2.Vect())) {
      h2d->Fill(v1.P(), v2.P());
      ++N_sel;
    }

  } // end of event loop

  cout << N_sel << "\t" << N_events << "\t" << 100.*N_sel/N_events << "%" << endl;

  //h2d->Draw("colz");

  h2d->Write();
  h2dt->Write();
  f.Close();

}