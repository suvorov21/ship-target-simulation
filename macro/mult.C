#include <iostream>

#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

float GetI(TH1F* h) {
  return 1.*h->Integral(-1., h->GetXaxis()->GetNbins()+1);
}


int mult(TString file_name) {
  auto f = new TFile(file_name, "READ");

  auto norm = 1.5e6;
  // 46 * 1000
  norm = 15.*1000.;

  auto t = (TTree*)f->Get("SHiP_kaon");

  auto h = new TH1F("h", "", 2, 0, 2);

  auto K_a_born   = (TH1F*)f->Get("Kaon_born_inside");

  auto K_p_born   = (TH1F*)f->Get("KaonP_born");
  auto K_p_DAR    = (TH1F*)f->Get("KaonP_decay_at_rest");
  auto K_p_DAF    = (TH1F*)f->Get("KaonP_decay_at_flight");

  auto K_p_int    = (TH1F*)f->Get("KaonP_interacts_wo_nu");
  auto K_p_leave    = (TH1F*)f->Get("KaonP_leave");

  auto K_int    = (TH1F*)f->Get("Kaon_interacts_wo_nu");
  auto K_leave    = (TH1F*)f->Get("Kaon_leave");

  cout << "Born per proton" << endl;

  cout << "Kaon born " << GetI(K_a_born) / norm << endl;
  cout << "Kaon DAF  " << 1.*t->Project("h", "1", "Kaon_Ekin > 0.005") / norm << endl;
  cout << "Kaon DAR  " << 1.*t->Project("h", "1", "Kaon_Ekin < 0.005") / norm << endl;
  cout << "Kaon int  " << GetI(K_int) / norm << endl;
  cout << "Kaon exit " << GetI(K_leave) / norm << endl;

  cout << endl;

  cout << "Kaon P born " << GetI(K_p_born) / norm << endl;
  cout << "Kaon P DAF  " << GetI(K_p_DAF) / norm << endl;
  cout << "Kaon P DAR  " << GetI(K_p_DAR) / norm << endl;
  cout << "Kaon P int  " << GetI(K_p_int) / norm << endl;
  cout << "Kaon P exit " << GetI(K_p_leave) / norm << endl;
  cout << "cross check " << endl;
  cout << "KpDAR       " << GetI(K_p_DAR) / norm << endl;
  cout << "KpDAF       " << GetI(K_p_DAF) / norm << endl;

  auto K_prod_lambda    = (TH1F*)f->Get("Kaon_produce_Lambda");
  auto K_prod_sigma     = (TH1F*)f->Get("Kaon_produce_Sigma");
  auto K_prod_kaonC      = (TH1F*)f->Get("Kaon_produce_Kaons_C");
  auto K_prod_kaonN      = (TH1F*)f->Get("Kaon_produce_Kaons_N");
  auto K_prod_kaonO      = (TH1F*)f->Get("Kaon_produce_Kaons_O");

  cout << endl;
  cout << "Born per Kaon" << endl;

  if (K_prod_lambda) {
    cout << "Kaon C " << GetI(K_prod_kaonC) / norm << endl;
    cout << "Kaon N " << GetI(K_prod_kaonN) / norm << endl;
    cout << "Kaon O " << GetI(K_prod_kaonO) / norm << endl;
    cout << "Sigma  " << GetI(K_prod_sigma) / norm << endl;
    cout << "Lambda " << GetI(K_prod_lambda) / norm << endl;
  }

  auto pion_prod = (TH1F*)f->Get("Pion_born_inside");
  cout << endl;
  cout << "pion prod " << GetI(pion_prod) / norm << endl;

  auto Kaon_int_not_to_kaon = (TH1F*)f->Get("Kaon_int_not_to_kaon");
  auto Kaon_born_not_by_kaon = (TH1F*)f->Get("Kaon_born_not_by_kaon");

  if (Kaon_born_not_by_kaon) {
    cout << "kaon born w/o ini K " << GetI(Kaon_born_not_by_kaon) / norm << endl;
    cout << "kaon int w/o kaon " << GetI(Kaon_int_not_to_kaon) / norm << endl;
  }


  auto ppKp_leave = (TH1F*)f->Get("ppKp_leave");
  auto ppKn_leave = (TH1F*)f->Get("ppKn_leave");

  if (ppKp_leave) {
    cout << "pp K pos leave" << GetI(ppKp_leave) / norm << endl;
    cout << "pp K neg leave" << GetI(ppKn_leave) / norm << endl;
  }


  return 1;

}