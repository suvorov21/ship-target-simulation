{
  auto f = new TFile("SHiP_sim_tot_prelim_v2.root", "READ");
  auto t = (TTree*)f->Get("SHiP_kaon");
  double z;
  t->SetBranchAddress("kaon_pos_z", &z);

  std::vector<double> v;

  auto N = t->GetEntries();
  for (auto i = 0; i < N; ++i) {
    t->GetEntry(i);
    v.push_back(z);
  }

  std::sort(v.begin(), v.end());

  auto c = 0;
  for (auto it = v.begin(); it != v.end()-1; ++it)
    if (*(it) == *(it+1))
      ++c;

  std::cout << "Total    : \t" << N << std::endl;
  std::cout << "Doubling : \t" << c << std::endl;

}