# SHiP target simulation package

Simple G4 simulation package for the SHiP target.

## Ouput
Decay kaon and pion spectra

output_root_file.root:

    |-- TTree SHiP_pion     # contains the information about pion decays
        |-- Pion_Ekin       # pion final kinetic energy [MeV]
        |-- pion_dir_x      # pion direction x
        |-- pion_dir_y      # pion direction y
        |-- pion_dir_z      # pion direction z
        |-- pion_pos_x      # pion decay position x [mm]
        |-- pion_pos_y      # pion decay position y [mm]
        |-- pion_pos_z      # pion decay position z [mm]

    |-- TH2F Pion_decays            # pion final momentum-direction [MeV]
    |-- TH1F Pion_born_inside       # initial momentum of the pions born in the target
    |-- TH1F Pion_decay_inside      # initial momentum of the pions decaying in the target (born also inside the target)
    |-- TH1F Pion_leave             # initial momentum of the pions leaving the target
    |-- TH1F Pion_born_outside      # Initial momentum of the pions born outside the target

## References
- Target geometry http://arxiv.org/abs/1810.06880
