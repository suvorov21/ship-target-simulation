//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SHiP_DetectorConstruction.cc
/// \brief Implementation of the SHiP_DetectorConstruction class

#include "SHiP_DetectorConstruction.hh"
//#include "SHiP_MagneticField.hh"

#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4Mag_UsualEqRhs.hh"

#include "G4Material.hh"
#include "G4Element.hh"
#include "G4MaterialTable.hh"
#include "G4NistManager.hh"

#include "G4VSolid.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4PVReplica.hh"
#include "G4UserLimits.hh"

#include "G4SDManager.hh"
#include "G4VSensitiveDetector.hh"
#include "G4RunManager.hh"
#include "G4GenericMessenger.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//G4ThreadLocal SHiP_MagneticField* SHiP_DetectorConstruction::fMagneticField = 0;
//G4ThreadLocal G4FieldManager* SHiP_DetectorConstruction::fFieldMgr = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_DetectorConstruction::SHiP_DetectorConstruction()
: G4VUserDetectorConstruction(),
  fVisAttributes(),
  fCheckOverlaps(true)
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_DetectorConstruction::~SHiP_DetectorConstruction()
{
  for (auto visAttributes: fVisAttributes) {
    delete visAttributes;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* SHiP_DetectorConstruction::Construct()
{

  ConstructMaterials();

  // Geometry parameters
  auto  worldSizeXY = 40 * m;
  auto  worldSizeZ  = 40 * m;

  auto targ_XY = 30 * cm;

  auto water_thikness = 5*mm;
  // detector layers
  const auto N_tzm = 13;
  float tzm_thikness[N_tzm]  = {8*cm, 2.5*cm, 2.5*cm, 2.5*cm, 2.5*cm, 2.5*cm, 2.5*cm,
                               2.5*cm, 5*cm, 5*cm, 6.5*cm, 8*cm, 8*cm};
  float tzm_pos[N_tzm];

  // start from the beginning od the detector
  // water layer with thikness 5 mm between each layer
  tzm_pos[0] = tzm_thikness[0] / 2.;
  for (auto i = 1; i < N_tzm; ++i)
    tzm_pos[i] = water_thikness + tzm_pos[i-1] + tzm_thikness[i-1]/2. +  tzm_thikness[i]/2.;

  const auto N_tung = 4;
  float tung_thikness[N_tung] = {5*cm, 8*cm, 10*cm, 35*cm};
  float tung_pos[N_tung];

  tung_pos[0] = tzm_pos[N_tzm-1] + tzm_thikness[N_tzm-1]/2. + water_thikness + tung_thikness[0]/2.;
  for (auto i = 1; i < N_tung; ++i)
    tung_pos[i] = water_thikness + tung_pos[i-1] +tung_thikness[i-1]/2. + tung_thikness[i]/2.;

  float water_pos[N_tzm + N_tung - 1];
  for (auto i = 0; i < N_tzm; ++i)
    water_pos[i] = tzm_pos[i] + tzm_thikness[i]/2. + water_thikness/2.;
  for (auto i = 0; i < N_tung - 1; ++i)
    water_pos[N_tzm + i] = tung_pos[i] + tung_thikness[i]/2. + water_thikness/2.;

  // Get materials
  auto defaultMaterial = G4Material::GetMaterial("G4_AIR");
  auto tungstenMaterial = G4Material::GetMaterial("tungsten");
  auto tzmMaterial = G4Material::GetMaterial("TZM");
  auto waterMaterial = G4Material::GetMaterial("Water");

  if ( ! defaultMaterial || ! tungstenMaterial || ! tzmMaterial || ! waterMaterial ) {
    G4ExceptionDescription msg;
    msg << "Cannot retrieve materials already defined.";
    G4Exception("B4DetectorConstruction::DefineVolumes()",
      "MyCode0001", FatalException, msg);
  }

  //
  // World
  //
  auto worldS
    = new G4Box("World",           // its name
                 worldSizeXY/2, worldSizeXY/2, worldSizeZ/2); // its size

  auto worldLV
    = new G4LogicalVolume(
                 worldS,           // its solid
                 defaultMaterial,  // its material
                 "World");         // its name

  auto worldPV
    = new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(),  // at (0,0,0)
                 worldLV,          // its logical volume
                 "World",          // its name
                 0,                // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps

  //
  // TZM layers
  //

  G4Box* tzm_box[N_tzm];

  for (auto i = 0; i < N_tzm; ++i)
    tzm_box[i]
      = new G4Box("TZM_" + std::to_string(i),     // its name
                   targ_XY/2, targ_XY/2, tzm_thikness[i]/2); // its size

  G4LogicalVolume* tzm_LV[N_tzm];
  for (auto i = 0; i < N_tzm; ++i)
    tzm_LV[i]
      = new G4LogicalVolume(
                  tzm_box[i],    // its solid
                  tzmMaterial, // its material
                  "TZM_" + std::to_string(1));  // its name

  for (auto i = 0; i < N_tzm; ++i)
    new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(0., 0., tzm_pos[i]),  // at (0,0,0)
                 tzm_LV[i],          // its logical volume
                 "TZM_" + std::to_string(i),    // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps

  //
  // Tungsten layers
  //

  G4Box* tung_box[N_tung];

  for (auto i = 0; i < N_tung; ++i)
    tung_box[i]
      = new G4Box("TUNG_" + std::to_string(i),     // its name
                   targ_XY/2, targ_XY/2, tung_thikness[i]/2); // its size

  G4LogicalVolume* tung_LV[N_tung];
  for (auto i = 0; i < N_tung; ++i)
    tung_LV[i]
      = new G4LogicalVolume(
                  tung_box[i],    // its solid
                  tungstenMaterial, // its material
                  "TUNG_" + std::to_string(1));  // its name

  for (auto i = 0; i < N_tung; ++i)
    new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(0., 0., tung_pos[i]),  // at (0,0,0)
                 tung_LV[i],          // its logical volume
                 "TUNG_" + std::to_string(i),    // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps

  //
  // Water layers
  //

  G4Box* water_box[N_tzm + N_tung - 1];

  for (auto i = 0; i < N_tzm + N_tung - 1; ++i)
    water_box[i]
      = new G4Box("WATER_" + std::to_string(i),     // its name
                   targ_XY/2, targ_XY/2, water_thikness/2); // its size

  G4LogicalVolume* water_LV[N_tzm + N_tung - 1];
  for (auto i = 0; i < N_tzm + N_tung - 1; ++i)
    water_LV[i]
      = new G4LogicalVolume(
                  water_box[i],    // its solid
                  waterMaterial, // its material
                  "WATER_" + std::to_string(1));  // its name

  for (auto i = 0; i < N_tzm + N_tung - 1; ++i)
    new G4PVPlacement(
                 0,                // no rotation
                 G4ThreeVector(0., 0., water_pos[i]),  // at (0,0,0)
                 water_LV[i],          // its logical volume
                 "WATER_" + std::to_string(i),    // its name
                 worldLV,          // its mother  volume
                 false,            // no boolean operation
                 0,                // copy number
                 fCheckOverlaps);  // checking overlaps


  // print parameters
  //
  G4cout << "TZM pos:\t";
  for (auto i = 0; i < N_tzm; ++i)
    G4cout <<  tzm_pos[i] << "\t";
  G4cout << G4endl;

  G4cout << "TZM thikness:\t";
  for (auto i = 0; i < N_tzm; ++i)
    G4cout <<  tzm_thikness[i] << "\t";
  G4cout << G4endl;

  G4cout << "Tung pos:\t";
  for (auto i = 0; i < N_tung; ++i)
    G4cout <<  tung_pos[i] << "\t";
  G4cout << G4endl;

  G4cout << "Tung thikness:\t";
  for (auto i = 0; i < N_tung; ++i)
    G4cout <<  tung_thikness[i] << "\t";
  G4cout << G4endl;

  G4cout << "Water pos:\t";
  for (auto i = 0; i < N_tzm + N_tung - 1; ++i)
    G4cout <<  water_pos[i] << "\t";
  G4cout << G4endl;


  //
  // Visualization attributes
  //
  worldLV->SetVisAttributes (G4VisAttributes::GetInvisible());


  // visualization attributes ------------------------------------------------


  auto visAttributes = new G4VisAttributes(G4Colour(0.9, 0.9, 0.9));   // LightGray
  for (auto i = 0; i < N_tzm; ++i)
    tzm_LV[i]->SetVisAttributes(visAttributes);
  fVisAttributes.push_back(visAttributes);

  visAttributes = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5)); // dark gray
  for (auto i = 0; i < N_tung; ++i)
    tung_LV[i]->SetVisAttributes(visAttributes);
  fVisAttributes.push_back(visAttributes);

  visAttributes = new G4VisAttributes(G4Colour(0. , 0., 9.)); // Blue
  for (auto i = 0; i < N_tung + N_tzm - 1; ++i)
    water_LV[i]->SetVisAttributes(visAttributes);
  fVisAttributes.push_back(visAttributes);



  return worldPV;
}


void SHiP_DetectorConstruction::ConstructMaterials()
{
  auto nistManager = G4NistManager::Instance();

  // Air
  nistManager->FindOrBuildMaterial("G4_AIR");

  G4double a;  // mass of a mole;
  G4double z;  // z=mean number of protons;
  G4double density;
  G4String symbol;
  G4int ncomp;

  // Tungsten
  new G4Material("tungsten", z=74., a= 183.84*g/mole, density= 19.3*g/cm3);

  // TZM (molybdenum)
  new G4Material("TZM",  z=42., a= 95.94*g/mole, density= 10.22*g/cm3);

  auto d = 1.01*g/mole;
  G4Element* ele_H = new G4Element("Hydrogen", symbol="H", z=1., d);
  d = 16.00*g/mole;
  G4Element* ele_O = new G4Element("Oxygen", symbol="O", z=8., d);

  G4Material* H2O = new G4Material("Water", density=1.*g/cm3, ncomp=2);
  G4int natoms;
  H2O->AddElement(ele_H, natoms=2);
  H2O->AddElement(ele_O, natoms=1);


  G4cout << G4endl << "The materials defined are : " << G4endl << G4endl;
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}

