//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SHiP_EventAction.cc
/// \brief Implementation of the SHiP_EventAction class

#include "SHiP_EventAction.hh"
#include "SHiP_Constants.hh"
#include "SHiP_Analysis.hh"
#include "SHiP_Trajectory.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"

using std::array;
using std::vector;


namespace {

// Utility function which finds a hit collection with the given Id
// and print warnings if not found
/*G4VHitsCollection* GetHC(const G4Event* event, G4int collId) {
  auto hce = event->GetHCofThisEvent();
  if (!hce) {
      G4ExceptionDescription msg;
      msg << "No hits collection of this event found." << G4endl;
      G4Exception("SHiP_EventAction::EndOfEventAction()",
                  "SHiP_Code001", JustWarning, msg);
      return nullptr;
  }

  auto hc = hce->GetHC(collId);
  if ( ! hc) {
    G4ExceptionDescription msg;
    msg << "Hits collection " << collId << " of this event not found." << G4endl;
    G4Exception("SHiP_EventAction::EndOfEventAction()",
                "SHiP_Code001", JustWarning, msg);
  }
  return hc;
}*/

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_EventAction::SHiP_EventAction()
: G4UserEventAction()
  //fHodHCID  {{ -1, -1 }},
  //fDriftHCID{{ -1, -1 }},
  //fCalHCID  {{ -1, -1 }},
  //fDriftHistoID{{ {{ -1, -1 }}, {{ -1, -1 }} }},
  //fCalEdep{{ vector<G4double>(kNofEmCells, 0.), vector<G4double>(kNofHadCells, 0.) }}
      // std::array<T, N> is an aggregate that contains a C array.
      // To initialize it, we need outer braces for the class itself
      // and inner braces for the C array
{
  // set printing per each event
  G4RunManager::GetRunManager()->SetPrintProgress(1);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_EventAction::~SHiP_EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SHiP_EventAction::BeginOfEventAction(const G4Event*)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SHiP_EventAction::EndOfEventAction(const G4Event* event)
{
  //
  // Fill histograms & ntuple
  //
  (void)event;
  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // study how often pions/kaons leave the target
  auto TrajCont = event->GetTrajectoryContainer();

  // loop to eal with kaons / pions
  for (uint trajID = 0; trajID < TrajCont->entries(); ++trajID) {
    auto traj = (SHiP_Trajectory*)(*TrajCont)[trajID];
    auto particle_PDG = traj->GetPDGEncoding();

    if (traj->GetMoreThenOneKaon() && abs(particle_PDG) == 321)
      analysisManager->FillH1(27, traj->GetInitialMomentum().mag());

    /*if (particle_PDG == 2212) {
      if (traj->kaon_pair.first != -1 && traj->kaon_pair.second != -1) {
        SHiP_Trajectory* traj1 = GetTrajByID(traj->kaon_pair.first, event);
        SHiP_Trajectory* traj2 = GetTrajByID(traj->kaon_pair.second, event);

        if (traj1 && traj2) {

          if (traj1->GetFinalVolumeName() == "World" &&
            traj1->GetFinalProcessName() == "Transportation")
            analysisManager->FillH1(25, traj->GetInitialMomentum().mag());

          if (traj2->GetFinalVolumeName() == "World" &&
            traj2->GetFinalProcessName() == "Transportation")
            analysisManager->FillH1(26, traj->GetInitialMomentum().mag());
        }
      }
    }*/

    if (traj->GetInitialVolumeName() == "TZM_0") {
      if (particle_PDG == 321) {
        analysisManager->FillH1(12, traj->GetInitialMomentum().mag());
        analysisManager->FillH2(3, acos(traj->GetInitialMomentum().z() / traj->GetInitialMomentum().mag()), traj->GetInitialMomentum().mag());
      } else if (particle_PDG == 111)
        analysisManager->FillH1(13, traj->GetInitialMomentum().mag());
    }

    // store K+ and k- production points
    if (particle_PDG == 321)
      analysisManager->FillH2(6, traj->GetInitialPosition().z(), traj->GetInitialPosition().y());
    else if (particle_PDG == -321)
      analysisManager->FillH2(7, traj->GetInitialPosition().z(), traj->GetInitialPosition().y());

    // interested in pions and kaons
    if (abs(particle_PDG) == 211) {
      if (traj->GetInitialVolumeName() != "World") {
        analysisManager->FillH1(0, traj->GetInitialMomentum().mag());
        if (traj->GetFinalVolumeName() == "World")
          analysisManager->FillH1(2, traj->GetInitialMomentum().mag());
      } else
        analysisManager->FillH1(3, traj->GetInitialMomentum().mag());
    } else if (abs(particle_PDG) == 321) {
      if (traj->GetInitialVolumeName() != "World") {
        analysisManager->FillH1(4, traj->GetInitialMomentum().mag());
        if (NotProducedByKaonSameCharge(traj, event))
          analysisManager->FillH1(23, traj->GetInitialMomentum().mag());
        analysisManager->FillH2(5, traj->GetInitialPosition().z(), traj->GetInitialPosition().y());
        if (particle_PDG == 321) {
          analysisManager->FillH1(11, traj->GetInitialMomentum().mag());
          analysisManager->FillH2(2, acos(traj->GetInitialMomentum().z() / traj->GetInitialMomentum().mag()), traj->GetInitialMomentum().mag());
        }
        if (traj->GetFinalVolumeName() == "World") {
          analysisManager->FillH1(6, traj->GetInitialMomentum().mag());
          if (traj->GetInitialMomentum().theta() < 0.1)
            analysisManager->FillH1(14, traj->GetInitialMomentum().mag());
        }
      } else
        analysisManager->FillH1(7, traj->GetInitialMomentum().mag());
    }

    // particle leave the volume
    if (traj->GetFinalVolumeName() == "World" && traj->GetFinalProcessName() == "Transportation") {
      if (abs(particle_PDG) == 321)
        analysisManager->FillH1(6, traj->GetInitialMomentum().mag());

      if (particle_PDG == 321) {
        analysisManager->FillH1(15, traj->GetInitialMomentum().mag());
        analysisManager->FillH2(4, traj->GetFinalMomentum().unit().z(), traj->GetFinalMomentum().mag());
      }
      continue;
    }

    // trajectory interacts not in the decay or capture
    if (traj->GetFinalProcessName() != "Decay" && traj->GetFinalProcessName() != "hBertiniCaptureAtRest") {
      if (traj->GetFinalVolumeName() != "World") {
        if (abs(particle_PDG) == 321) {
          analysisManager->FillH1(16, traj->GetInitialMomentum().mag());
          if (traj->GetProduceLambda())
            analysisManager->FillH1(17, traj->GetInitialMomentum().mag());
          if (traj->GetProduceSigma())
            analysisManager->FillH1(18, traj->GetInitialMomentum().mag());
          if (traj->GetProduceKaonC())
            analysisManager->FillH1(19, traj->GetInitialMomentum().mag());
          if (traj->GetProduceKaonN())
            analysisManager->FillH1(20, traj->GetInitialMomentum().mag());
          if (traj->GetProduceKaonO())
            analysisManager->FillH1(21, traj->GetInitialMomentum().mag());
        }
        if (particle_PDG == 321) {
          analysisManager->FillH1(8, traj->GetInitialMomentum().mag());
        } else if (particle_PDG == 411) {
          analysisManager->FillH1(13, traj->GetInitialMomentum().mag());
        } else if (particle_PDG == 521) {
          analysisManager->FillH1(17, traj->GetInitialMomentum().mag());
        }

        if ((particle_PDG == 321 && !traj->GetProduceKaonPos()) ||
            (particle_PDG == -321 && !traj->GetProduceKaonNeg()))
          analysisManager->FillH1(24, traj->GetInitialMomentum().mag());
      }
      continue;
    }

    G4ThreeVector MomDir = traj->GetFinalMomentum();
    if (MomDir.mag())
      MomDir /= MomDir.mag();
    else
      MomDir = G4ThreeVector(0., 0., 0.);
    G4ThreeVector Position = traj->GetFinalPosition();
    G4float Ekin = traj->GetFinalEnergy();

    // Fill histoes
    if (abs(particle_PDG) == 211) {
      analysisManager->FillNtupleDColumn(0, 0, Ekin);
      analysisManager->FillNtupleDColumn(0, 1, MomDir.x());
      analysisManager->FillNtupleDColumn(0, 2, MomDir.y());
      analysisManager->FillNtupleDColumn(0, 3, MomDir.z());
      analysisManager->FillNtupleDColumn(0, 4, Position.x());
      analysisManager->FillNtupleDColumn(0, 5, Position.y());
      analysisManager->FillNtupleDColumn(0, 6, Position.z());

      if (particle_PDG > 0.)
        analysisManager->FillNtupleIColumn(0, 7, 1.);
      else
        analysisManager->FillNtupleIColumn(0, 7, -1.);

      analysisManager->AddNtupleRow(0); // pion
      analysisManager->FillH2(0, MomDir.z(), traj->GetFinalMomentum().mag());

      // Fill decays inside/outside the target
      if (traj->GetFinalVolumeName() != "World" && traj->GetInitialVolumeName() != "World")
        analysisManager->FillH1(1, traj->GetInitialMomentum().mag());
    }

    if (abs(particle_PDG) == 321) {
      analysisManager->FillNtupleDColumn(1, 0, Ekin);
      analysisManager->FillNtupleDColumn(1, 1, MomDir.x());
      analysisManager->FillNtupleDColumn(1, 2, MomDir.y());
      analysisManager->FillNtupleDColumn(1, 3, MomDir.z());
      analysisManager->FillNtupleDColumn(1, 4, Position.x());
      analysisManager->FillNtupleDColumn(1, 5, Position.y());
      analysisManager->FillNtupleDColumn(1, 6, Position.z());

      if (particle_PDG > 0.) {
        analysisManager->FillNtupleIColumn(1, 7, 1.);
        if (Ekin < 0.001)
          analysisManager->FillH1(9, traj->GetInitialMomentum().mag()); // KDAR
        else
          analysisManager->FillH1(10, traj->GetInitialMomentum().mag()); // KDAF
      } else
        analysisManager->FillNtupleIColumn(1, 7, -1.);

      analysisManager->AddNtupleRow(1); // kaon
      analysisManager->FillH2(1, MomDir.z(), traj->GetFinalMomentum().mag());

      if (traj->GetFinalVolumeName() != "World" && traj->GetInitialVolumeName() != "World")
        analysisManager->FillH1(5, traj->GetInitialMomentum().mag());
    }
  } // loop over trajectories in the event
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
bool SHiP_EventAction::NotProducedByKaonSameCharge(const SHiP_Trajectory* traj,
                                    const G4Event* event) {
  auto TrajCont = event->GetTrajectoryContainer();

  for (uint trajID = 0; trajID < TrajCont->entries(); ++trajID) {
    auto traj_tmp     = (SHiP_Trajectory*)(*TrajCont)[trajID];
    auto particle_PDG = traj_tmp->GetPDGEncoding();
    if (abs(particle_PDG) != 321)
      continue;

    if (particle_PDG * traj->GetPDGEncoding() > 0 &&
        traj->GetParentID() == traj_tmp->GetTrackID())
      return true;
  }

  return false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
SHiP_Trajectory* SHiP_EventAction::GetTrajByID(const G4int ID,
  const G4Event* event) {
  auto TrajCont = event->GetTrajectoryContainer();

  for (uint trajID = 0; trajID < TrajCont->entries(); ++trajID) {
    auto traj_tmp     = (SHiP_Trajectory*)(*TrajCont)[trajID];
    if (traj_tmp->GetTrackID() == ID)
      return traj_tmp;
  }

  return NULL;

}
