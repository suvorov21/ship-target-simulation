#ifndef SHiP_Trajectory_h
#define SHiP_Trajectory_h 1

#include "G4Trajectory.hh"
#include "G4Allocator.hh"
#include "G4ios.hh"
#include "globals.hh"
#include "G4ParticleDefinition.hh"
#include "G4TrajectoryPoint.hh"
#include "G4Track.hh"
#include "G4Step.hh"

class G4Polyline;                   // Forward declaration.

class SHiP_Trajectory : public G4Trajectory
{
  public:

    SHiP_Trajectory();
    SHiP_Trajectory(const G4Track* aTrack);
    SHiP_Trajectory(SHiP_Trajectory &);
    virtual ~SHiP_Trajectory();

    inline void* operator new(size_t);
    inline void  operator delete(void*);

    //G4int GetPDG() {return PDG;}
    //G4float GetInitialEnergy() {return InitialEnergy;}
    G4float GetFinalEnergy() {return FinalEnergy;}

    //G4ThreeVector GetInitialMomentum() {return InitialMomentum;}
    G4ThreeVector GetFinalMomentum() {return FinalMomentum;}

    G4ThreeVector GetInitialPosition() {return InitialPosition;}
    G4ThreeVector GetFinalPosition() {return FinalPosition;}

    G4String GetFinalProcessName() {return FinalProcessName;}
    G4String GetFinalVolumeName() {return FinalVolumeName;}
    G4String GetInitialVolumeName() {return InitialVolumeName;}

    void SetInitialPosition(G4ThreeVector vect) {InitialPosition = vect;}
    void SetFinalPosition(G4ThreeVector vect) {FinalPosition = vect;}

    void SetFinalMomentum(G4ThreeVector vect) {FinalMomentum = vect;}

    void SetFinalEnergy(G4float e) {FinalEnergy = e;}

    void SetProduceNu(bool var)     {ProduceNu = var;}
    void SetProduceLambda(bool var) {ProduceLambda = var;}
    void SetProduceSigma(bool var)  {ProduceSigma = var;}
    void SetProduceKaonC(bool var)   {ProduceK_c = var;}
    void SetProduceKaonN(bool var)   {ProduceK_n = var;}
    void SetProduceKaonO(bool var)   {ProduceK_o = var;}
    void SetProduceKaonPos(bool var) {ProduceKpos = var;}
    void SetProduceKaonNeg(bool var) {ProduceKneg = var;}
    void SetMoreThenOneKaon(bool var) {MoreThenOneKaon = var;}

    bool GetProduceNu()     {return ProduceNu;}
    bool GetProduceLambda() {return ProduceLambda;}
    bool GetProduceSigma()  {return ProduceSigma;}
    bool GetProduceKaonC()   {return ProduceK_c;}
    bool GetProduceKaonN()   {return ProduceK_n;}
    bool GetProduceKaonO()   {return ProduceK_o;}
    bool GetProduceKaonPos() {return ProduceKpos;}
    bool GetProduceKaonNeg() {return ProduceKneg;}
    bool GetMoreThenOneKaon() {return MoreThenOneKaon;}

    void SetFinalProcessName(G4String s) {FinalProcessName = s;}
    void SetFinalVolumeName(G4String s) {FinalVolumeName = s;}
    void SetInitialVolumeName(G4String s) {InitialVolumeName = s;}

    std::pair<G4int, G4int> kaon_pair;

  private:
    //G4int PDG;

    //G4float InitialEnergy;
    G4float FinalEnergy;

    bool ProduceNu;
    bool ProduceLambda;
    bool ProduceSigma;
    bool ProduceK_c;
    bool ProduceK_n;
    bool ProduceK_o;

    bool ProduceKpos;
    bool ProduceKneg;

    bool MoreThenOneKaon;

    //G4ThreeVector InitialMomentum;
    G4ThreeVector FinalMomentum;

    G4ThreeVector InitialPosition;
    G4ThreeVector FinalPosition;

    G4String FinalProcessName;

    G4String InitialVolumeName;
    G4String FinalVolumeName;
};

extern G4ThreadLocal G4Allocator<SHiP_Trajectory>* SHiP_TrajectoryAllocator;

inline void* SHiP_Trajectory::operator new(size_t)
{
  if(!SHiP_TrajectoryAllocator)
      SHiP_TrajectoryAllocator = new G4Allocator<SHiP_Trajectory>;
  return (void*)SHiP_TrajectoryAllocator->MallocSingle();
}

inline void SHiP_Trajectory::operator delete(void* aTrajectory)
{
  SHiP_TrajectoryAllocator->FreeSingle((SHiP_Trajectory*)aTrajectory);
}

#endif