//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SHiP_RunAction.cc
/// \brief Implementation of the SHiP_RunAction class

#include "SHiP_RunAction.hh"
#include "SHiP_EventAction.hh"
#include "SHiP_Analysis.hh"

#include "G4Run.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_RunAction::SHiP_RunAction(SHiP_EventAction* eventAction)
 : G4UserRunAction(),
   fEventAction(eventAction)
{
  // Create analysis manager
  // The choice of analysis technology is done via selectin of a namespace
  // in SHiP_Analysis.hh
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;

  // Default settings
  analysisManager->SetNtupleMerging(true);
     // Note: merging ntuples is available only with Root output
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetFileName("SHiP_Sim");

  // Book histograms, ntuple
  //

  // Creating 1D histograms
  // 1) Produced in the target
  // 2) Decay in the target
  // 3) Produced outside
   // h1 Id = 0
  analysisManager
    ->CreateH1("Pion_born_inside",
              "Pion born in the target initial momentum",
               100, 0., 10000);
   // h1 Id = 1
  analysisManager
    ->CreateH1("Pion_decay_inside",
              "Pion decay in the target (born in target) initial momentum",
               100, 0., 10000);
   // h1 Id = 2
  analysisManager
    ->CreateH1("Pion_leave",
              "Pion leave the target initial momentum",
               100, 0., 10000);
   // h1 Id = 3
  analysisManager
    ->CreateH1("Pion_born_outside",
              "Pion born outside of the target initial momentum",
               100, 0., 10000);

   // h1 Id = 4
  analysisManager
    ->CreateH1("Kaon_born_inside",
              "Kaon born in the target initial momentum",
               100, 0., 10000);
   // h1 Id = 5
  analysisManager
    ->CreateH1("Kaon_decay_inside",
              "Kaon decay in the target (born in target) initial momentum",
               100, 0., 10000);
   // h1 Id = 6
  analysisManager
    ->CreateH1("Kaon_leave",
              "Kaon leave the target initial momentum",
               100, 0., 10000);
   // h1 Id = 7
  analysisManager
    ->CreateH1("Kaon_born_outside",
              "Kaon born outside the target initial momentum",
               100, 0., 10000);

   // h1 Id = 8
  analysisManager
    ->CreateH1("KaonP_interacts_wo_nu",
              "Kaon interacts wo decay",
               100, 0., 10000);

   // h1 Id = 9
  analysisManager
    ->CreateH1("KaonP_decay_at_rest",
              "Kaon decay at rest",
               100, 0., 10000);

  // h1 Id = 10
  analysisManager
    ->CreateH1("KaonP_decay_at_flight",
              "Kaon decay at flight",
               100, 0., 10000);

  // h1 Id = 11
  analysisManager
    ->CreateH1("KaonP_born",
              "Positive kaon born in target",
               100, 0., 10000);

  // h1 Id = 12
  analysisManager
    ->CreateH1("KaonP_born_fst_layer",
              "Positive kaon born in 1st layer (8cm molybdenium)",
               200, 0., 20000);

  // h1 Id = 13
  analysisManager
    ->CreateH1("PiO_born_fst_layer",
              "Neutral pion born in 1st layer (8cm molybdenium)",
               200, 0., 20000);

  // h1 Id = 14
  analysisManager
    ->CreateH1("Kaon_leave_fwd",
              "Kaon leaving the target fwd going (100 mrad)",
               200, 0., 20000);

  // h1 Id = 15
  analysisManager
    ->CreateH1("KaonP_leave",
              "Positive kaon leaving the target",
               200, 0., 20000);

  // h1 Id = 16
  analysisManager
    ->CreateH1("Kaon_interacts_wo_nu",
              "Kaons interacts w/o decay",
               200, 0., 20000);

  // h1 Id = 17
  analysisManager
    ->CreateH1("Kaon_produce_Lambda",
              "Kaons produce Lambda",
               200, 0., 20000);
  // h1 Id = 18
  analysisManager
    ->CreateH1("Kaon_produce_Sigma",
              "Kaons produce Sigma",
               200, 0., 20000);
  // h1 Id = 19
  analysisManager
    ->CreateH1("Kaon_produce_Kaons_C",
              "Kaons produce Kaons charged",
               200, 0., 20000);
  // h1 Id = 20
  analysisManager
    ->CreateH1("Kaon_produce_Kaons_N",
              "Kaons produce Kaons neutral",
               200, 0., 20000);
  // h1 Id = 21
  analysisManager
    ->CreateH1("Kaon_produce_Kaons_O",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 22
  analysisManager
    ->CreateH1("Kaon_interacts_wo_nu_wu_ch_kaon",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 23
  analysisManager
    ->CreateH1("Kaon_born_not_by_kaon",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 24
  analysisManager
    ->CreateH1("Kaon_int_not_to_kaon",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 25
  analysisManager
    ->CreateH1("ppKp_leave",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 26
  analysisManager
    ->CreateH1("ppKn_leave",
              "Kaons produce Kaons other",
               200, 0., 20000);

  // h1 Id = 27
  analysisManager
    ->CreateH1("MoreKaon",
              "Kaons produce Kaons other",
               200, 0., 20000);

    // TODO implement
    // 1. K -> K + X(no kaon)   kaon should be same charge
    // 2. p + p -> K^+ + K^- + X check the number of kaons that exits


  // Creating 2D histograms
   // h2 Id = 0
  analysisManager
    ->CreateH2("Pion_decays",
              "Pion decays momentum angle",
               102, -1.01, 1.01, 200, 0., 70000);
   // h2 Id = 1
  analysisManager
    ->CreateH2("Kaon_decays",
              "Kaon decays momentum angle",
               102, -1.01, 1.01, 200, 0., 10000);

   // h2 Id = 2
  analysisManager
    ->CreateH2("KaonP_prod",
              "Positive kaon production momentum angle",
               1000, 0., 3.15, 1000, 0., 50000);

   // h2 Id = 3
  analysisManager
    ->CreateH2("KaonP_prod_fst_layer",
              "Positive kaon production at 1st layer momentum angle",
               1000, 0., 3.15, 1000, 0., 50000);

   // h2 Id = 4
  analysisManager
    ->CreateH2("Kaon_leave_2d",
              "Kaon leave the bunker kinematics",
               102, -1.01, 1.01, 200, 0., 70000);

   // h2 Id = 5
  analysisManager
    ->CreateH2("Kaon_prod",
              "Kaon production point",
               140, 0., 1400., 100, -500., 500);

   // h2 Id = 6
  analysisManager
    ->CreateH2("KaonP_prod_point",
              "Kaon production point",
               140, 0., 1400., 100, -500., 500);

   // h2 Id = 7
  analysisManager
    ->CreateH2("KaonN_prod_point",
              "Kaon production point",
               140, 0., 1400., 100, -500., 500);

  // Creating ntuple
  //
  if ( fEventAction ) {
    auto N_id = analysisManager->CreateNtuple("SHiP_pion", "Pion");
    analysisManager->CreateNtupleDColumn("Pion_Ekin");    // N_id = 0   column Id = 0
    analysisManager->CreateNtupleDColumn("pion_dir_x");   // N_id = 0   column Id = 1
    analysisManager->CreateNtupleDColumn("pion_dir_y");   // N_id = 0   column Id = 2
    analysisManager->CreateNtupleDColumn("pion_dir_z");   // N_id = 0   column Id = 3
    analysisManager->CreateNtupleDColumn("pion_pos_x");   // N_id = 0   column Id = 4
    analysisManager->CreateNtupleDColumn("pion_pos_y");   // N_id = 0   column Id = 5
    analysisManager->CreateNtupleDColumn("pion_pos_z");   // N_id = 0   column Id = 6
    analysisManager->CreateNtupleIColumn("pion_charge");  // N_id = 0   column Id = 7

    analysisManager->FinishNtuple(N_id);

    N_id = analysisManager->CreateNtuple("SHiP_kaon", "Kaon");
    analysisManager->CreateNtupleDColumn("Kaon_Ekin");    // N_id = 1   column Id = 0
    analysisManager->CreateNtupleDColumn("kaon_dir_x");   // N_id = 1   column Id = 1
    analysisManager->CreateNtupleDColumn("kaon_dir_y");   // N_id = 1   column Id = 2
    analysisManager->CreateNtupleDColumn("kaon_dir_z");   // N_id = 1   column Id = 3
    analysisManager->CreateNtupleDColumn("kaon_pos_x");   // N_id = 1   column Id = 4
    analysisManager->CreateNtupleDColumn("kaon_pos_y");   // N_id = 1   column Id = 5
    analysisManager->CreateNtupleDColumn("kaon_pos_z");   // N_id = 1   column Id = 6
    analysisManager->CreateNtupleIColumn("kaon_charge");  // N_id = 0   column Id = 7

    analysisManager->FinishNtuple(N_id);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_RunAction::~SHiP_RunAction()
{
  delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SHiP_RunAction::BeginOfRunAction(const G4Run* /*run*/)
{
  //inform the runManager to save random number seed
  //G4RunManager::GetRunManager()->SetRandomNumberStore(true);

  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  // The default file name is set in SHiP_RunAction::SHiP_RunAction(),
  // it can be overwritten in a macro
  analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SHiP_RunAction::EndOfRunAction(const G4Run* /*run*/)
{
  // save histograms & ntuple
  //
  auto analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
