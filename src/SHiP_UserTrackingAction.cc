////////////////////////////////////////////////////////////
// $Id: SHiP_UserTrackingAction.cc,v 1.6 2007/01/01 05:32:49 mcgrew Exp $
//

#include "globals.hh"
#include "G4Track.hh"
#include "G4TrackingManager.hh"

#include "SHiP_UserTrackingAction.hh"
#include "SHiP_Trajectory.hh"

SHiP_UserTrackingAction::SHiP_UserTrackingAction() {}

SHiP_UserTrackingAction::~SHiP_UserTrackingAction() {}

void SHiP_UserTrackingAction::PreUserTrackingAction(const G4Track* trk) {
    G4VTrajectory* traj = new SHiP_Trajectory(trk);
    fpTrackingManager->SetTrajectory(traj);
    if (abs(traj->GetPDGEncoding()) == 211 ||
        abs(traj->GetPDGEncoding()) == 321 ||
        traj->GetPDGEncoding() == 2212)
      fpTrackingManager->SetStoreTrajectory(true);

    //G4cout << traj->GetPointEntries() << G4endl; //GetRange();
}

void SHiP_UserTrackingAction::PostUserTrackingAction(const G4Track* trk) {
  SHiP_Trajectory* trajectory =
    (SHiP_Trajectory*)fpTrackingManager->GimmeTrajectory();

  auto step = trk->GetStep();

  auto TrackPoint = step->GetPreStepPoint();
  if (TrackPoint == NULL)
    return;

  G4String process_name_post = step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();

  const std::vector<const G4Track*>* fSecondary = step->GetSecondaryInCurrentStep();
  G4int tN2ndariesTot = (*fSecondary).size();
  G4int daughterK = 0;
  for(size_t lp1=(*fSecondary).size()-tN2ndariesTot; lp1<(*fSecondary).size(); lp1++){
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3122)
      trajectory->SetProduceLambda(true);
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3222 ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3212 ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3112 ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3224 ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3214 ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 3114)
      trajectory->SetProduceSigma(true);
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 130  ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 310  ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 311)
      trajectory->SetProduceKaonN(true);
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 321) {
      ++daughterK;
      trajectory->kaon_pair.first = (*fSecondary)[lp1]->GetTrackID();
      trajectory->SetProduceKaonC(true);
      trajectory->SetProduceKaonPos(true);
    }
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == -321) {
      ++daughterK;
      trajectory->kaon_pair.second = (*fSecondary)[lp1]->GetTrackID();
      trajectory->SetProduceKaonC(true);
      trajectory->SetProduceKaonNeg(true);
    }
    if ((*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 313  ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 323  ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 315  ||
        (*fSecondary)[lp1]->GetDefinition()->GetPDGEncoding() == 325)
      trajectory->SetProduceKaonO(true);

    if ((*fSecondary)[lp1]->GetDefinition()->GetParticleName().contains("nu"))
      trajectory->SetProduceNu(true);
  }

  if (daughterK > 1)
    trajectory->SetMoreThenOneKaon(true);

  G4double energy_pre = TrackPoint->GetKineticEnergy();

  auto MomDir     = TrackPoint->GetMomentumDirection();
  MomDir /= MomDir.mag();
  auto Position   = TrackPoint->GetPosition();
  auto Momentum   = TrackPoint->GetMomentum();
  auto VolumeName = TrackPoint->GetPhysicalVolume()->GetName();

  trajectory->SetFinalPosition(Position);
  trajectory->SetFinalEnergy(energy_pre);
  trajectory->SetFinalMomentum(Momentum);
  trajectory->SetFinalProcessName(process_name_post);
  trajectory->SetFinalVolumeName(VolumeName);
}

