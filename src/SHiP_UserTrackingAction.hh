////////////////////////////////////////////////////////////
// $Id: SHiP_UserTrackingAction.hh,v 1.3 2004/03/18 20:49:27 t2k Exp $
//
#ifndef SHiP_UserTrackingAction_h
#define SHiP_UserTrackingAction_h 1

#include "G4UserTrackingAction.hh"
class G4Track;

class SHiP_UserTrackingAction : public G4UserTrackingAction
{
  public:
    SHiP_UserTrackingAction();
    virtual ~SHiP_UserTrackingAction();

    virtual void PreUserTrackingAction(const G4Track*);
    virtual void PostUserTrackingAction(const G4Track*);
};
#endif
