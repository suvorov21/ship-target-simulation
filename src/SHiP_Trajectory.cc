#include "SHiP_Trajectory.hh"
#include "G4TrajectoryPoint.hh"
#include "G4Trajectory.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleTypes.hh"
#include "G4ThreeVector.hh"
#include "G4Polyline.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4VVisManager.hh"
#include "G4Polymarker.hh"

G4ThreadLocal G4Allocator<SHiP_Trajectory>* SHiP_TrajectoryAllocator = nullptr;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_Trajectory::SHiP_Trajectory()
  :G4Trajectory(),
  ProduceNu(false),
  ProduceLambda(false),
  ProduceK_c(false),
  ProduceK_n(false),
  ProduceK_o(false),
  ProduceKpos(false),
  ProduceKneg(false),
  MoreThenOneKaon(false) {
  //fParticleDefinition = nullptr;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_Trajectory::SHiP_Trajectory(const G4Track* aTrack)
  :G4Trajectory(aTrack)
{
  //fParticleDefinition=aTrack->GetDefinition();
  //PDG = aTrack->GetParticleDefinition()->GetPDGEncoding();
  InitialPosition = aTrack->GetPosition();
  InitialVolumeName = aTrack->GetVolume()->GetName();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_Trajectory::SHiP_Trajectory(SHiP_Trajectory &right)
  :G4Trajectory(right)
{
  //fParticleDefinition=right.fParticleDefinition;
  //PDG = right.PDG;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SHiP_Trajectory::~SHiP_Trajectory() {}